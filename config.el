;; Littering

(use-package no-littering
  :demand
  :config
  (with-eval-after-load 'recentf
    (add-to-list 'recentf-exclude no-littering-var-directory)
    (add-to-list 'recentf-exclude no-littering-etc-directory)))

(global-display-line-numbers-mode)
(setq display-time-default-load-average nil)
;; (display-battery-mode 1)

;; y & n for yes or no

(fset 'yes-or-no-p 'y-or-n-p)

(setq inhibit-startup-message t)

(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)


;; Org Superstar

(use-package org-superstar
	     :config
	     (setq org-superstar-special-todo-items t)
	     (add-hook 'org-mode-hook (lambda ()
					(org-superstar-mode 1))))
(setq initial-major-mode 'org-mode)
(setq org-hide-emphasis-markers t)

;; Org-Roam

(use-package org-roam
	     :straight t
	     :custom
	     (org-roam-directory (file-truename "~/orgroam"))
	     :bind (("C-c n l" . org-roam-buffer-toggle)
		    ("C-c n f" . org-roam-node-find)
		    ("C-c n g" . org-roam-graph)
		    ("C-c n i" . org-roam-node-insert)
		    ("C-c n c" . org-roam-capture)
		    ;; Dailies
		    ("C-c n j" . org-roam-dailies-capture-today))
	     :config
	     (org-roam-db-autosync-mode)
	     ;; If using org-roam-protocol
	     (require 'org-roam-protocol))
(setq org-roam-v2-ack t)

;; Org-roam-ui

(use-package websocket
	     :straight t)
(use-package simple-httpd
	     :straight t)
(use-package f
	     :straight t)
(use-package org-roam-ui
	     :straight t)
(add-to-list 'load-path "~/.emacs.d/private/org-roam-ui")
(load-library "org-roam-ui")

;; Org-reveal

(use-package ox-reveal
	     :straight t)
(setq  org-reveal-root "https://cdn.jsdelivr.net/npm/reveal.js")
(use-package htmlize
	     :straight t)

;; Org-Appear

(use-package org-appear
	     :hook (org-mode . org-appear-mode))
(setq org-pretty-entities t)
(setq org-startup-with-inline-images t
      org-image-actual-width '(600))
(setq org-src-fontify-natively t)
(setq org-src-tab-acts-natively t)
(add-to-list 'org-structure-template-alist
	     '("el" . "src emacs-lisp"))



 ;; Org Capture
(custom-set-variables
 '(org-directory "~/org"))
(global-set-key (kbd "C-c c") 'org-capture)

;; Org-capture templates

(setq org-capture-templates
      '(("s" "Snippet" entry
	 (file+headline "mysnip.org" "My Snippets")
	 "* %^{note :} \n %? %i")))

;; Icons

(use-package all-the-icons
	     :straight t)

(use-package all-the-icons-dired
	     :straight t
	     :custom-face (all-the-icons-dired-dir-face ((t (:foreground nil))))
	     :hook (dired-mode . all-the-icons-dired-mode))
;; Theme

(use-package exotica-theme
	     :straight t)

(use-package underwater-theme
	     :straight t)

(use-package cyberpunk-theme
	     :straight t)

(use-package laguna-theme
	     :straight t)

(use-package sublime-themes
	     :straight t)


(use-package klere-theme
  :straight t)

;; Hide ModeLine

(use-package hide-mode-line
	     :commands (hide-mode-line-mode))

;; Keycast

(use-package keycast
  :config 
  (define-minor-mode keycast-mode
    "Show current command and its key binding in the mode line."
    :global t
    (if keycast-mode
	(add-hook 'pre-command-hook 'keycast--update t)
      (remove-hook 'pre-command-hook 'keycast--update)))
  (add-to-list 'global-mode-string '("" mode-line-keycast " "))
  (keycast-mode))

;; Transparency

(defun toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
		    ((numberp (cdr alpha)) (cdr alpha))
		    ;; Also handle undocumented (<active> <inactive>) form.
		    ((numberp (cadr alpha)) (cadr alpha)))
	      100)
	 '(85 . 50) '(100 . 100)))))
(global-set-key (kbd "C-c t") 'toggle-transparency)

;; Modeline

(use-package telephone-line
  :straight t)
(setq telephone-line-primary-left-separator 'telephone-line-cubed-left
      telephone-line-secondary-left-separator 'telephone-line-cubed-hollow-left
      telephone-line-primary-right-separator 'telephone-line-cubed-right
      telephone-line-secondary-right-separator 'telephone-line-cubed-hollow-right)
(setq telephone-line-height 24
      telephone-line-evil-use-short-tag t)


(telephone-line-mode 1)

(use-package mode-icons
  :straight t)
(mode-icons-mode)

;; Which key
(use-package which-key
  :straight t
  :config (which-key-mode))

;; Avy

(use-package avy
  :straight t
  :bind ("M-s" . avy-goto-char))

;; Projectile

(use-package projectile
  :straight t
  :config
  (define-key projectile-mode-map (kbd "C-x p") 'Projectile-command-map)
  (projectile-mode +1))

;; Treemacs

(use-package treemacs
  :straight t
  :bind
  (:map global-map
	([f8] . treemacs))
  :config
  (setq treemacs-is-never-other-window t))

(use-package treemacs-projectile
  :straight t)

;; Company Mode

(use-package company
  :straight t
  :init
  (add-hook 'after-init-hook 'global-company-mode))

;; Flycheck

(use-package flycheck
  :straight t
  :init
  (global-flycheck-mode t))

;; Expand region

(use-package expand-region
  :straight t
  :bind
  ("C-=" . er/expand-region)
  ("C--" . er/contract-region))


;; Smart Parens

(use-package smartparens
  :straight t)
(smartparens-mode t)


;; Selectrum
(use-package selectrum
  :straight t)
(selectrum-mode +1)

(use-package selectrum-prescient
  :straight t)
(selectrum-prescient-mode +1)
(prescient-persist-mode +1)

;; Marginalia
(use-package marginalia
  :straight t)
(marginalia-mode 1)


;; Embark

(use-package embark
  :straight t
  :bind
  (("C-." . embark-act)         
   ("C-;" . embark-dwim)        
   ("C-h B" . embark-bindings)) 

  :init
  (setq prefix-help-command #'embark-prefix-help-command)

  :config
  (add-to-list 'display-buffer-alist
               '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                 nil
                 (window-parameters (mode-line-format . none)))))

(use-package embark-consult
  :straight t
  :after (embark consult)
  :demand t 
  :hook
  (embark-collect-mode . consult-preview-at-point-mode))

;; Consult

(use-package consult
  :straight t
  :bind
  ("C-x b" . consult-buffer)
  ("C-s" . consult-line))

;; Magit

(use-package magit
  :straight t)
